﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class Door : MonoBehaviour
{
    [TagSelector] public String playerTag = "Player";
    
    // event triggered when player enters door
    public UnityEvent onPlayerEnterEvent;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(playerTag))
        {
            onPlayerEnterEvent.Invoke();  
        }
    }
}
