﻿using System;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacterController2D))]
public class Player : MonoBehaviour
{
    public CharacterController2D controller;
    public Animator animator;

    // tag specifying deadly objects in map
    [TagSelector] public String deadlyTag = "Deadly";
    
    [Header("Events")]
    public UnityEvent onPlayerDeathEvent;

    private float _horizontalMove = 0f;
    private bool _jumpPressed = false;
    private bool _runPressed = false;

    private static readonly int IsRunningId = Animator.StringToHash("isRunning");
    private static readonly int IsJumpingId = Animator.StringToHash("isJumping");

    private void Awake()
    {
        // subscribe to jump/land event
        controller.onJumpEvent.AddListener(
            (isJumping) => animator.SetBool(IsJumpingId, isJumping));
    }

    private void Update()
    {
        _horizontalMove = Input.GetAxisRaw("Horizontal");
        animator.SetBool(IsRunningId, Mathf.Abs(_horizontalMove) > 0);

        _jumpPressed |= Input.GetButtonDown("Vertical")
                       && Input.GetAxis("Vertical") > 0
                       || Input.GetButtonDown("Jump");

        _runPressed = Input.GetButton("Run");
    }

    private void FixedUpdate()
    {
        controller.Move(_horizontalMove * Time.deltaTime,
            run: _runPressed,
            jump: _jumpPressed
            );
        _jumpPressed = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // if player touches spikes die
        if (other.CompareTag(deadlyTag)) onPlayerDeathEvent.Invoke();
    }
}
