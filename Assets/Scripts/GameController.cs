﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public void RestartLevel()
    {
        Debug.Log("Restarting game");
        // reload the same scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGame()
    {
        Debug.Log("Quitting game");
        
#if UNITY_EDITOR
        // if in editor stop play mode
        UnityEditor.EditorApplication.isPlaying = false;
#else
        // if stand alone application quit application
        Application.Quit();
#endif
    }

    public void NextLevel()
    {
        Debug.Log("Going to next level");
        // load next scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}