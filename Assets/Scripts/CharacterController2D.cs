using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class BoolEvent : UnityEvent<bool> { }

/**
 * A modified version of the CharacterController2D in
 * https://github.com/Brackeys/2D-Character-Controller
 * with performance improvements and support for wall-jumping
 *
 * @author Raheeb Hassan
 */
[RequireComponent(typeof(Rigidbody2D))]
public class CharacterController2D : MonoBehaviour
{
    [Header("Movement")] 
    public float walkSpeed = 40f;
    public float runSpeed = 70f;
    public bool canRun = true;
    [Range(0, .3f)] public float movementSmoothing = .05f; // How much to smooth out the movement

    [Header("Jump")]
    public float jumpForce = 400f; // Amount of force added when the player jumps.
    public bool airControl = false; // Whether or not a player can steer while jumping;
    public Transform groundCheck; // A position marking where to check if the player is grounded.
    [Help("Ground Layer (what is ground) must not contain the Player object")]
    public LayerMask whatIsGround; // A mask determining what is ground to the character

    [Header("Crouch")]
    [Range(0, 1)] public float crouchSpeedMultiplier = .36f; // Amount of maxSpeed applied to crouching movement. 1 = 100%
    public Transform ceilingCheck; // A position marking where to check for ceilings
    public Collider2D crouchDisableCollider; // A collider that will be disabled when crouching

    [Header("Events")] [Space] 
    public BoolEvent onJumpEvent;
    public BoolEvent onCrouchEvent;

    private const float GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    private const float CeilingRadius = .2f; // Radius of the overlap circle to determine if the player can stand up
    private const float VelocityMultiplier = 10f;

    private Rigidbody2D _rigidbody;
    private Transform _transform;
    private Vector2 _acceleration = Vector2.zero;
    private bool _isFacingRight = true; // For determining which way the player is currently facing.
    private bool _isGrounded = true; // Whether or not the player is grounded.
    private bool _wasCrouching = false;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _transform = transform;
        if (onJumpEvent == null) onJumpEvent = new BoolEvent();
        if (onCrouchEvent == null) onCrouchEvent = new BoolEvent();
    }

    private void FixedUpdate()
    {
        GroundedCheck();
    }

    public void Move(float move, bool run = false, bool crouch = false, bool jump = false)
    {
        // modify speed based on whether he is running or walking
        // running is allowed only on ground
        move *= canRun && run ? runSpeed : walkSpeed;
        
        // If crouching, check to see if the character can stand up
        if (!crouch)
        {
            // If the character has a ceiling preventing them from standing up, keep them crouching
            if (Physics2D.OverlapCircle(ceilingCheck.position, CeilingRadius, whatIsGround)) crouch = true;
        }

        // only control the player if grounded or airControl is turned on
        if (_isGrounded || airControl)
        {
            // If crouching
            if (crouch)
            {
                if (!_wasCrouching)
                {
                    _wasCrouching = true;
                    onCrouchEvent.Invoke(true);
                }

                // Reduce the speed by the crouchSpeed multiplier
                move *= crouchSpeedMultiplier;

                // Disable one of the colliders when crouching
                if (crouchDisableCollider != null)
                    crouchDisableCollider.enabled = false;
            }
            else
            {
                // Enable the collider when not crouching
                if (crouchDisableCollider != null)
                    crouchDisableCollider.enabled = true;

                if (_wasCrouching)
                {
                    _wasCrouching = false;
                    onCrouchEvent.Invoke(false);
                }
            }

            // Move the character by finding the target velocity
            Vector2 velocity = _rigidbody.velocity;
            Vector2 targetVelocity = new Vector2(move * VelocityMultiplier, velocity.y);
            // And then smoothing it out and applying it to the character
            _rigidbody.velocity = Vector2.SmoothDamp(velocity, targetVelocity, ref _acceleration, movementSmoothing);

            // If the input is moving the player right and the player is facing left...
            if (move > 0 && !_isFacingRight)
            {
                // ... flip the player.
                Flip();
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (move < 0 && _isFacingRight)
            {
                // ... flip the player.
                Flip();
            }
        }

        // If the player should jump...
        if (_isGrounded && jump)
        {
            // Add a vertical force to the player.
            // _isGrounded = false;
            _rigidbody.AddForce(new Vector2(0f, jumpForce));
        }
    }


    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        _isFacingRight = !_isFacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = _transform.localScale;
        theScale.x *= -1;
        _transform.localScale = theScale;
    }

    private void GroundedCheck()
    {
        bool wasGrounded = _isGrounded;
        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        _isGrounded = Physics2D.OverlapCircle(groundCheck.position, GroundedRadius, whatIsGround);
        
        // if grounded status changed, fire jump/land event
        if (_isGrounded != wasGrounded) onJumpEvent.Invoke(!_isGrounded);
    }
}